# Jupyter Explore Music Visualization

Through Jupyter notebook, a piece of music can be compiled into a video of Visualization by compiling Explore Music


## Description
Through the content of deep-music-visualizer, I repeatedly adjusted the pitch intensity and smoothness of the video, and changed the image vector by changing the speed and pitch intensity accordingly. The music can be continuously ordered and written to form an exciting Visualization<br>
By using jupyter, more markdown than python can be marked in the code page


## Installation
Clone deep-music-visualizer project 
Install Anaconda3 and run visualize.py from the Anaconda virtual environment


## Usage

Different instructions correspond to different operations. Within a certain range, the vector of the video changes with the instructions
![image.png](./image.png)
%run visualize.py --song 20220215.wav --resolution 512 --duration 30 --pitch_sensitivity 270 --depth 0.8 --num_classes 3  --classes 25 77 987 234 87 69 775 345 52 353 242 975 --sort_classes_by_power 1 --jitter 0.2 --frame_length 1024 --truncation 0.7 --smooth_factor 15 --batch_size 25 --use_previous_classes 1 --output_file dk_movie.mp4<br>

I used the command to continuously control the frame_length, through a high rate to achieve the video visualization of fast changing music<br>
In particular, I changed the depth and classes, and through continuous exploration, I got the best video effect to show this beautiful piano piece<br>

## Authors and acknowledgment
Thanks to Orwa and the creators of  deep-music-visualizer<br>
Let us come into contact with jupyter and python to write code, and create exciting visual exploration works

